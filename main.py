#!/usr/bin/env python3
import sys


class FibbonacciNumber:
    def __init__(self, number: int):
        """ Ctor
        :param number: Number from fibbonacci sequence
        """
        if not self.__is_in_fibbonacci_sequence(number):
            raise ValueError("Given number is not in Fibbonacci sequence")
        self.value = number

    def __is_in_fibbonacci_sequence(self, number: int) -> bool:
        if not isinstance(number, int):
            raise TypeError("Incorrect number type")
        if number < 0:
            raise ValueError("Non positive number")
        x = 0
        y = 1
        if (number == x) or (number == y):
            return True
        n = x + y
        while n < number:
            x = y
            y = n
            n = x + y

        if number == n:
            return True
        return False


value = input("Give me number an I say you it is in Fibbonacci sequence: ")
if not value.isdecimal():
    print("You do not five a number!", file=sys.stderr)
    sys.exit(1)

value = int(value)
try:
    a = FibbonacciNumber(value)
except (ValueError, TypeError) as e:
    print(e, file=sys.stderr)
else:
    print("{} is in Fibonacci sequence".format(a.value))
